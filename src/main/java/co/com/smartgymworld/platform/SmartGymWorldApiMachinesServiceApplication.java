package co.com.smartgymworld.platform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartGymWorldApiMachinesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartGymWorldApiMachinesServiceApplication.class, args);
	}

}
